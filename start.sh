#!/bin/bash
####Pre-install###
TOKEN=PASTE_YOUR_TOKEN

##APT INSTALL###

apt update && apt install -y tomcat9 default-jre curl wget
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
dpkg -i gitlab-runner_amd64.deb
systemctl start tomcat9
systemctl enable tomcat9

ufw allow 8080 ## if needed


###OPTIONAL for certbot and nginx###
apt install -y certbot nginx python3-certbot-nginx gnupg ca-certificates lsb-release
certbot run -n --nginx --agree-tos -d test-link.xfin.net  -m  my-test-link@gmail.com  --redirect
rm /etc/nginx/sites-enabled/default
cp ${PWD}/.config/default /etc/nginx/sites-enabled/test-link.xfin.net
systemctl restart tomcat9
systemctl restart nginx
mkdir /var/lib/tomcat9/webapps/test
chown -R  gitlab-runner:tomcat /var/lib/tomcat9

rm gitlab-runner_amd64.deb
gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com" \
  --registration-token ${TOKEN} \
  --description "link-test" \
  --executor "shell"
